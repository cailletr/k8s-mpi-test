FROM debian:stretch-slim as build

WORKDIR /tmp

RUN apt-get update && apt-get install -y --no-install-recommends \
    gcc \
    make \
    libopenmpi-dev
COPY makefile mpi_hello_world.c /tmp/
RUN make


FROM debian:stretch-slim

RUN apt-get update && apt-get install -y --no-install-recommends \
    openmpi-bin \
    ssh

COPY --from=build /tmp/mpi_hello_world /usr/local/bin 

USER 1000
CMD mpirun /usr/local/bin/mpi_hello_world
